package com.kozpinar.challenge.common;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by onurkozpinar on 18.12.2017.
 */

public class Model {

    public static List<String> mainList = Arrays.asList(
            "Rider Profile",
            "asdasda");



    public static LineData lineDataSet(int count, float range, int color, int fillColor) {

        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < count; i++) {
            float mult = (range + 1);
            float val = (float) (Math.random() * mult) + 20 + (float) ((mult * 0.1) / 10);
            values.add(new Entry(i, val));
        }
        LineDataSet set = new LineDataSet(values, "");

        set.setDrawIcons(false);

        set.setCubicIntensity(1f);

        set.setCircleColor(color);
        set.setLineWidth(1f);
        set.setColor(fillColor);
        set.setCircleRadius(4f);
        set.disableDashedLine();
        set.setFillAlpha(255);
        set.setDrawFilled(true);
        set.setFormLineWidth(0f);
        set.setValueTextSize(14);

        set.setDrawValues(false);



        set.setFillColor(fillColor);


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set);


        return new LineData(dataSets);
    }

    public static PieData pieDataSet(int count, float range, int[] colors) {
        ArrayList<PieEntry> values = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            values.add(new PieEntry((float) ((Math.random() * range) + range / 5),""));
        }

        PieDataSet dataSet = new PieDataSet(values, "");


        dataSet.setDrawIcons(false);

        dataSet.setDrawValues(false);
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(0f);
        dataSet.setColors(colors);
        dataSet.setValueLineWidth(0f);
        dataSet.setValueLineVariableLength(false);


        PieData pieData = new PieData(dataSet);


        pieData.setHighlightEnabled(false);

        return pieData;
    }

    public static SpannableString generateSpannableText(CharSequence charSequence, int color) {

        SpannableString s = new SpannableString(charSequence);
        s.setSpan(new RelativeSizeSpan(1.7f), 0, s.length(), 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(color), 0, s.length(), 0);
        return s;
    }

}
