package com.kozpinar.challenge.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kozpinar.challenge.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by onurkozpinar on 18.12.2017.
 */
public class MainListRecyclerViewAdapter extends RecyclerView.Adapter<MainListRecyclerViewAdapter.MainListViewHolder> {
    private List<String> items;

    private MainListOnClickListener onClickListener;

    MainListRecyclerViewAdapter(List<String> items) {
        this.items = items;
    }


    @Override
    public MainListViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_list_row, parent, false);
        return new MainListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MainListViewHolder holder, int position) {
        String item = items.get(position);
        holder.textView.setText(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onClick(holder.getAdapterPosition(),
                            items.get(holder.getAdapterPosition()));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void setOnClickListener(MainListOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    class MainListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textView)
        TextView textView;


        MainListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    interface MainListOnClickListener {
        void onClick(int position, String item);
    }
}