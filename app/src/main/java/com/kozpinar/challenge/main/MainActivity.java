package com.kozpinar.challenge.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kozpinar.challenge.R;
import com.kozpinar.challenge.common.Model;
import com.kozpinar.challenge.riderProfile.RiderProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private MainListRecyclerViewAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        this.setup();
    }

    private void setup() {
        listAdapter = new MainListRecyclerViewAdapter(Model.mainList);

        listAdapter.setOnClickListener(new MainListRecyclerViewAdapter.MainListOnClickListener() {
            @Override
            public void onClick(int position, String item) {
                if (position == 0) {
                    Intent intent = new Intent(getApplicationContext(), RiderProfileActivity.class);
                    startActivity(intent);
                }
            }
        });

        recyclerView.setAdapter(this.listAdapter);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager =  new LinearLayoutManager(getApplicationContext());
        layoutManager.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());



    }


}
