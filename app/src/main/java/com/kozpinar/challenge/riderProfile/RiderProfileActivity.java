package com.kozpinar.challenge.riderProfile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.kozpinar.challenge.R;
import com.kozpinar.challenge.common.Model;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RiderProfileActivity extends AppCompatActivity {

    @BindView(R.id.bottomLineChart)
    LineChart bottomLineChart;

    @BindView(R.id.scorePieChart)
    PieChart scorePieChart;

    @BindView(R.id.topLineChart)
    LineChart topLineChart;

    @BindView(R.id.topPieChart)
    PieChart topPieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_profile);
        ButterKnife.bind(this);
        setup();

    }

    private void setup() {
        setupBottomLineChart();
        setupScorePieChart();
        setupTopLineChart();
        setupTopPieChart();
    }

    private void setupBottomLineChart() {

        bottomLineChart.setData(Model.lineDataSet(10, 100,
                getResources().getColor(R.color.colorOrange),
                getResources().getColor(R.color.colorOrangeOpaque)));

        bottomLineChart.setViewPortOffsets(0,10,0,0);
        bottomLineChart.getLegend().setEnabled(false);
        bottomLineChart.setDrawGridBackground(false);
        bottomLineChart.getDescription().setEnabled(false);
        bottomLineChart.setTouchEnabled(false);
        bottomLineChart.setPinchZoom(false);


        XAxis x = bottomLineChart.getXAxis();
        x.setEnabled(false);

        YAxis y = bottomLineChart.getAxisLeft();
        y.setEnabled(false);

        bottomLineChart.getAxisRight().setEnabled(false);


        bottomLineChart.animateXY(2000, 2000);

        bottomLineChart.invalidate();

    }

    private void setupScorePieChart() {
        scorePieChart.setData(Model.pieDataSet(2, 100, new int[] {
                getResources().getColor(R.color.colorOrange),
                Color.LTGRAY
        }));

        scorePieChart.setCenterText(Model.generateSpannableText("93", Color.WHITE));

        scorePieChart.setTouchEnabled(false);
        scorePieChart.getLegend().setEnabled(false);
        scorePieChart.getDescription().setEnabled(false);
        scorePieChart.setHoleColor(Color.LTGRAY);
        scorePieChart.setCenterTextColor(Color.WHITE);
        scorePieChart.setTransparentCircleAlpha(0);
        scorePieChart.animateXY(2000, 2000);

        scorePieChart.invalidate();
    }

    private void setupTopPieChart() {
        topPieChart.setData(Model.pieDataSet(2, 100, new int[] {
                getResources().getColor(R.color.colorOrange),
                Color.WHITE
        }));

        topPieChart.setCenterText("300$");
        topPieChart.setCenterTextColor(Color.LTGRAY);
        topPieChart.setCenterTextSize(12);
        topPieChart.setTransparentCircleAlpha(0);

        topPieChart.setTouchEnabled(false);
        topPieChart.getLegend().setEnabled(false);
        topPieChart.getDescription().setEnabled(false);

        topPieChart.animateXY(2000, 2000);

        topPieChart.invalidate();
    }

    private void setupTopLineChart() {
        topLineChart.setData(Model.lineDataSet(5, 100,
                getResources().getColor(R.color.colorOrange),
                getResources().getColor(R.color.colorOrangeOpaque)));

        topLineChart.setViewPortOffsets(0,10,20,0);
        topLineChart.getLegend().setEnabled(false);
        topLineChart.setDrawGridBackground(false);
        topLineChart.getDescription().setEnabled(false);
        topLineChart.setTouchEnabled(false);
        topLineChart.setPinchZoom(false);




        XAxis xAxis = topLineChart.getXAxis();
        xAxis.setTextSize(12f);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextColor(Color.LTGRAY);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis y = topLineChart.getAxisLeft();
        y.setEnabled(false);

        topLineChart.getAxisRight().setEnabled(false);
        topLineChart.animateXY(2000, 2000);

        topLineChart.invalidate();

    }
}
